#!/bin/bash
# sbatch power_ptest.cmd

#SBATCH --job-name="sequential_geomfinder_power"
#SBATCH -D .
#SBATCH --output=power-sequential_3_10.out
#SBATCH --error=power-sequential_3_10.err
#SBATCH --ntasks=1
##SBATCH --cpus-per-task=1
##SBATCH --gres=gpu:2
#SBATCH --time=00:02:00
./geomfinder 1EXP.pdb A 2O3P.pdb A 3 3 10 25 25 25 25 80 results/ n 50
