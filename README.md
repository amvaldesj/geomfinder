Ported C++ version from the original Python version.<br>

# Install compiler.

$ apt install g++

# Install a modified version of fpocket.

(not necessary if pocket search is not required.) Install a modified version of fpocket (https://github.com/Discngine/fpocket), allows selecting an output folder.

$ 7z x fpocket-3.0-master_modified.7z <br>
$ cd fpocket-3.0-master_modified <br>
$ make <br>
$ make install <br>

# Compiling kdtree library.

$ cd kdtree <br>
$ ./configure --enable-opt --disable-pthread --disable-fastalloc --disable-debug --prefix=. <br>
$ export CC=gcc <br>
$ make <br>

# Compiling geomfinder.

$ make <br>

# Running.

parameters: <br>

$ ./geomfinder P1 chain1 P2 chain2 gridSpacing umbralMin umbralMax nbondedPCT ascPCT tspPCT distancePCT scoreMin dirOut fpocket[y|n] minDrugscore <br>

# Example. 

Before create folder results/. <br>

$ mkdir results <br>
$ ./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50 <br>

one record in results/scores.json <br>

[{"radius1": 5, "radius2": 5, "scoref": 87.500000, "scoredistf": 83.333328, "scorenbondedf": 99.999985, "scoreASCf": 100.000000, "scoretspf": 66.666672, "resP1": 4, "resP2": 4, "site1": "LYS30 ASP34 TYR69 THR73", "site2": "ASP167 LYS169 THR204 TYR207", "respock1": "", "respock2": "", "scoreasc1": "F:D:B:C", "scoreasc2": "D:F:C:B", "prot1": "1EXP.pdb", "prot2": "2O3P.pdb", "chain1": "A", "chain2": "A", "Bsite1": "" , "Bsite2": "" }]
<br>

# Profiling.

Install valgrind and kcachegrind tools. Compile with -g parameter.<br>

$ valgrind --tool=callgrind ./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50 <br>
$ kcachegrind callgrind.out <br>

* memory usage: (memcheck, massif, or cachegrind) <br>
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/performance_tuning_guide/s-memory-valgrind <br>

# Download website.

$ git clone https://gitlab.com/amvaldesj/geomfinder.git -b django web <br>

#

--------

* original

./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50
time find_sites: 1EXP.pdb 0.0949263 seconds
time find_sites: 2O3P.pdb 0.0667657 seconds
compare sites sequential sites1: 484 sites2: 395
time compare_sites: 0.251994 seconds
scores: 1
time total: 0.436164 seconds

--------
* if (score_finger_dist > 0)

./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50
time find_sites: 1EXP.pdb 0.0935048 seconds
time find_sites: 2O3P.pdb 0.063807 seconds
compare sites sequential sites1: 484 sites2: 395
time compare_sites: 0.370453 seconds
scores: 1
time total: 0.558035 seconds

--------

* if (score_finger_dist > 0)
* if (score_finger_tsp > 0) 

./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50
time find_sites: 1EXP.pdb 0.0939838 seconds
time find_sites: 2O3P.pdb 0.060506 seconds
compare sites sequential sites1: 484 sites2: 395
time compare_sites: 0.359726 seconds
scores: 1
time total: 0.541815 seconds

--------

* if (score_finger_dist == 0) continue;
* if (score_finger_tsp == 0) continue;

./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50
time find_sites: 1EXP.pdb 0.0826554 seconds
time find_sites: 2O3P.pdb 0.0631359 seconds
compare sites sequential sites1: 484 sites2: 395
time compare_sites: 0.373008 seconds
scores: 1
time total: 0.530684 seconds

--------

* paramaters as macros. function without parameters.

./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50
0.250000 0.250000 0.250000 0.250000 0.800000
time find_sites: 1EXP.pdb 0.0814896 seconds
time find_sites: 2O3P.pdb 0.0640796 seconds
compare sites sequential sites1: 484 sites2: 395
time compare_sites: 0.266034 seconds
scores: 1
time total: 0.438799 seconds

--------

* using smin_prime for distancePCT.

./geomfinder 1EXP.pdb A 2O3P.pdb A 3 4 6 25 25 25 25 80 results/ n 50
time find_sites: 1EXP.pdb 0.0836125 seconds
time find_sites: 2O3P.pdb 0.0614949 seconds
compare sites sequential sites1: 484 sites2: 395
time compare_sites: 0.212038 seconds
scores: 1
time total: 0.369323 seconds

