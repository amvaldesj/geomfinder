/*
 * geomfinder.cpp
 */

#include <iostream>
using namespace std;
#include <fstream>
#include <sys/stat.h>
#include <cstring>

//
#include <chrono>
using namespace std::chrono;

//
#include "Protein.h"

/*
 * compares the sites.
 */
#ifdef OPT
vector<string> compare_sites(struct Protein protein1, struct Protein protein2, \
    struct Parameter params) {
        
    //
    auto start_compare = chrono::high_resolution_clock::now();
    
    //
    vector<string> scores;

    //
    int s1_size = protein1.total_sites;
    int s2_size = protein2.total_sites;
    
    //
    int comp = 0;
    
    //
    float smin_prime_tsp  = ((params.filterMax - (100 - params.tspPCT))/(params.tspPCT*0.01))*0.01;
    float smin_prime_dist = ((params.filterMax - (100 - params.distancePCT))/(params.distancePCT*0.01))*0.01;
    float smin_prime_nbe = ((params.filterMax - (100 - params.nbondedPCT))/(params.nbondedPCT*0.01))*0.01;
    float smin_prime_asc = ((params.filterMax - (100 - params.ascPCT))/(params.ascPCT*0.01))*0.01;
    
    //
    for (int i=0; i<s1_size; i++) {                
        for (int j=0; j<s2_size; j++) {
#ifdef TRACING
Extrae_event(200, j+1);
#endif              
                        
            // calculates the score.
            float score_finger_tsp = calc_score_TD(protein1.array_sites[i].finger_tsp, 
                                                protein2.array_sites[j].finger_tsp,
                                                protein1.array_sites[i].finger_tsp_size,
                                                protein2.array_sites[j].finger_tsp_size);
            if (score_finger_tsp < smin_prime_tsp) continue;

            //
            float score_finger_dist = calc_score_TD(protein1.array_sites[i].finger_dist, 
                                                protein2.array_sites[j].finger_dist, 
                                                protein1.array_sites[i].finger_dist_size, 
                                                protein2.array_sites[j].finger_dist_size);
            if (score_finger_dist < smin_prime_dist) continue;

            //
            float score_nbe = calc_score_NBE(protein1.array_sites[i].finger_nbonded, 
                                            protein2.array_sites[j].finger_nbonded);
            if (score_nbe < smin_prime_nbe) continue;

            //
            float score_finger_asc = calc_score_T(protein1.array_sites[i].finger_asc, 
                                                protein2.array_sites[j].finger_asc,
                                                protein1.array_sites[i].finger_asc_size,
                                                protein2.array_sites[j].finger_asc_size);
            if (score_finger_asc < smin_prime_asc) continue;

            // final score.
            float scoref = score_finger_dist * (params.distancePCT * 0.01) + score_nbe * 
                    (params.nbondedPCT * 0.01) + score_finger_asc * (params.ascPCT * 0.01) + 
                    score_finger_tsp * (params.tspPCT * 0.01);
            
            //
            if (scoref >= (params.filterMax * 0.01)) {
                string line = "{\"radius1\": " + to_string(protein1.array_sites[i].distance) +  ", " +
                           "\"radius2\": " + to_string(protein2.array_sites[j].distance) +  ", " +
                           "\"scoref\": " + to_string(scoref * 100) + 
                           ", \"scoredistf\": " + to_string(score_finger_dist * 100) + ", " +
                           "\"scorenbondedf\": " + to_string(score_nbe * 100) + 
                           ", \"scoreASCf\": " + to_string(score_finger_asc * 100) + ", " +
                           "\"scoretspf\": " + to_string(score_finger_tsp * 100) + 
                           ", \"resP1\": " + to_string(protein1.array_sites[i].size) + ", " +
                           "\"resP2\": " + to_string(protein2.array_sites[j].size) + ", " +
                           "\"site1\": \"" + protein1.array_sites[i].residues + 
                           "\", \"site2\": \"" + protein2.array_sites[j].residues + "\", " +
                           "\"respock1\": \"" + protein1.array_sites[i].residues_pocket + 
                           "\", \"respock2\": \"" + protein2.array_sites[j].residues_pocket + "\", " +
                           "\"scoreasc1\": \"" + protein1.array_sites[i].finger_asc + 
                           "\", \"scoreasc2\": \"" + protein2.array_sites[j].finger_asc + "\", " +
                           "\"prot1\": \"" + params.p1 + 
                           "\", \"prot2\": \"" + params.p2 + 
                           "\", \"chain1\": \"" + params.ch1 + 
                           "\", \"chain2\": \"" + params.ch2 + "\", " + 
                           "\"Bsite1\": \"" + protein1.array_sites[i].shared + "\" " + 
                           ", \"Bsite2\": \"" + protein2.array_sites[j].shared + "\" " + "}";
                    
                scores.push_back(line);
            }
#ifdef TRACING
Extrae_event(200, 0);
#endif
        
            comp++;
        }
    }
    
    //
    printf("total comparisons: %d\n", comp);
        
    //
    chrono::duration<float> elapsed = chrono::high_resolution_clock::now() - start_compare;
    cout << "time compare sites: " << elapsed.count() << " seconds" << endl;
    
    return scores;
}
#else
vector<string> compare_sites(struct Protein protein1, struct Protein protein2, struct Parameter params) {
    //
    auto start_compare = chrono::high_resolution_clock::now();
    
    //
    vector<string> scores;

    //
    int s1_size = protein1.total_sites;
    int s2_size = protein2.total_sites;
    
    //
    //int comp = 0;
    
    //
    for (int i=0; i<s1_size; i++) {        
        for (int j=0; j<s2_size; j++) {
#ifdef TRACING
Extrae_event(200, j+1);
#endif             
            // calculates the score.
            float score_finger_tsp = calc_score_TD(protein1.array_sites[i].finger_tsp, 
                                                protein2.array_sites[j].finger_tsp,
                                                protein1.array_sites[i].finger_tsp_size,
                                                protein2.array_sites[j].finger_tsp_size);

            //
            float score_nbe = calc_score_NBE(protein1.array_sites[i].finger_nbonded, 
                                            protein2.array_sites[j].finger_nbonded);

            //
            float score_finger_asc = calc_score_T(protein1.array_sites[i].finger_asc, 
                                                protein2.array_sites[j].finger_asc,
                                                protein1.array_sites[i].finger_asc_size,
                                                protein2.array_sites[j].finger_asc_size);

            //
            float score_finger_dist = calc_score_TD(protein1.array_sites[i].finger_dist, 
                                                protein2.array_sites[j].finger_dist, 
                                                protein1.array_sites[i].finger_dist_size, 
                                                protein2.array_sites[j].finger_dist_size);

            // final score.
            float scoref = score_finger_dist * (params.distancePCT * 0.01) + score_nbe * 
                    (params.nbondedPCT * 0.01) + score_finger_asc * (params.ascPCT * 0.01) + 
                    score_finger_tsp * (params.tspPCT * 0.01);
                
            //
            if (scoref >= (params.filterMax * 0.01)) {
                string line = "{\"radius1\": " + to_string(protein1.array_sites[i].distance) +  ", " +
                           "\"radius2\": " + to_string(protein2.array_sites[j].distance) +  ", " +
                           "\"scoref\": " + to_string(scoref * 100) + 
                           ", \"scoredistf\": " + to_string(score_finger_dist * 100) + ", " +
                           "\"scorenbondedf\": " + to_string(score_nbe * 100) + 
                           ", \"scoreASCf\": " + to_string(score_finger_asc * 100) + ", " +
                           "\"scoretspf\": " + to_string(score_finger_tsp * 100) + 
                           ", \"resP1\": " + to_string(protein1.array_sites[i].size) + ", " +
                           "\"resP2\": " + to_string(protein2.array_sites[j].size) + ", " +
                           "\"site1\": \"" + protein1.array_sites[i].residues + 
                           "\", \"site2\": \"" + protein2.array_sites[j].residues + "\", " +
                           "\"respock1\": \"" + protein1.array_sites[i].residues_pocket + 
                           "\", \"respock2\": \"" + protein2.array_sites[j].residues_pocket + "\", " +
                           "\"scoreasc1\": \"" + protein1.array_sites[i].finger_asc + 
                           "\", \"scoreasc2\": \"" + protein2.array_sites[j].finger_asc + "\", " +
                           "\"prot1\": \"" + params.p1 + 
                           "\", \"prot2\": \"" + params.p2 + 
                           "\", \"chain1\": \"" + params.ch1 + 
                           "\", \"chain2\": \"" + params.ch2 + "\", " + 
                           "\"Bsite1\": \"" + protein1.array_sites[i].shared + "\" " + 
                           ", \"Bsite2\": \"" + protein2.array_sites[j].shared + "\" " + "}";
                    
                scores.push_back(line);
            }
#ifdef TRACING
Extrae_event(200, 0);
#endif              
        
        //comp++;
        }
    }
    
    //
    //printf("total comparisons: %d\n", comp);

    //
    chrono::duration<float> elapsed = chrono::high_resolution_clock::now() - start_compare;
    cout << "time compare sites: " << elapsed.count() << " seconds" << endl;
    
    return scores;
}
#endif

/**/
void write_score_file(vector<string> scores, struct Parameter params) {
    bool first = true;
    ofstream mfile;
    string pathFile = params.dirOut + "/scores.json";
    
    mfile.open (pathFile);
    mfile << "[";
    
    for (auto item: scores) {
        if (first) {
            mfile << item;
            first = false;
        } else
            mfile << ",\n" << item;
    }
    mfile << "]";
    mfile.close();
}

/**/
void write_params_file(struct Parameter params) {
    ofstream mfile;
    string pathFile = params.dirOut + "/params.json";

    string line = "{\"protein1\": \"" + params.p1 + "\", \"protein2\": \"" + params.p2 + "\", ";
    line = line + "\"umbralMin\": " + to_string(params.umbralMin) + ", \"umbralMax\": " + to_string(params.umbralMax) + ", ";
    line = line + "\"filterMax\": " + to_string(params.filterMax) + ", \"nbondedPCT\": " + to_string(params.nbondedPCT) + ", ";
    line = line + "\"distancePCT\": " + to_string(params.distancePCT) + ", \"tspPCT\": " + to_string(params.tspPCT) + ", ";
    line = line + "\"ascPCT\": " + to_string(params.ascPCT) + ", \"gridPCT\": " + to_string(params.radius) + ", ";
    line = line + "\"fpocket\": \"" + params.fpocket + "\", ";
    line = line + "\"min_drugscore\": " + to_string(params.min_drugscore) + ", ";
    line = line + "\"chainP1\": \"" + params.ch1 + "\", \"chainP2\": \"" + params.ch2 + "\"}\n";

    mfile.open (pathFile);
    mfile << line;
    mfile.close();
}

/*
 * main function.
 */
int main (int argc , char *argv[]) {
    //
    chrono::duration<float> elapsed;
    auto start_t = chrono::high_resolution_clock::now();

    // gets the parameters.
    struct Parameter params;
    params.p1 = argv[1];
    params.ch1 = argv[2];
    params.p2 = argv[3];
    params.ch2 = argv[4];
    params.radius = atof(argv[5]);
    params.umbralMin = atof(argv[6]);
    params.umbralMax = atof(argv[7]);
    params.nbondedPCT = atof(argv[8]);
    params.ascPCT = atof(argv[9]);
    params.tspPCT = atof(argv[10]);
    params.distancePCT = atof(argv[11]);
    params.filterMax = atof(argv[12]);
    params.dirOut = argv[13];
    params.fpocket = argv[14];
    params.min_drugscore = atof(argv[15]);
       
    //
    struct Protein protein1;
    struct Protein protein2;

    //
    protein1 = prepare_protein(params.p1, params.ch1, params);
    protein2 = prepare_protein(params.p2, params.ch2, params);
    printf("sites1: %d sites2: %d\n", protein1.total_sites, protein2.total_sites);

    // final scores. compares the sites.
    vector<string> scores;
    
    //
    scores = compare_sites(protein1, protein2, params);
    
    // writes parameters in json file.
    write_params_file(params);
        
    // writes scores in json file.
    write_score_file(scores, params);
   
    //
    if (scores.size() == 0) {
        cout << "No results." << endl;
    } 

    //
    cout << "scores: " << scores.size() << endl;
    
    //
    elapsed = chrono::high_resolution_clock::now() - start_t;
    cout << "time total: " << elapsed.count() << " seconds" << endl;
    return 0;
}
