prefix=/usr/local
CC=g++
kdlib = kdtree/libkdtree.a

# normal x86
CFLAGS = -DOPT -std=c++11 -O3 -march=native -pipe 
#CFLAGS = -std=c++11 -O3 -march=native -pipe 
LDFLAGS = $(kdlib) -lm

# profiling (-pg for gprof), debugging (-g)
# -O3 optimize compilation, enable autovectorization.
###CFLAGS = -std=c++11 -O3 -march=native -pipe
###LDFLAGS = $(kdlib) -lm

SRC = geomfinder.cpp Protein.cpp Residue.cpp Atom.cpp Site.cpp utils.cpp Fpocket.cpp 
OBJ = geomfinder.o Protein.o Residue.o Atom.o Site.o utils.o Fpocket.o 
APP = geomfinder

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ) $(LIBS) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
