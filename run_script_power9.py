#!/usr/bin/env python

import subprocess

#
umin=3
umax=11
#
scripts = []

#
wall_clock_limit = "00:03:00"

# create scripts.
for max in range(5, umax + 1, 1):
    script="""#!/bin/bash
# sbatch utalca_psequential_{umin}_{max}.cmd

#SBATCH --job-name="sequential_geomfinder_utalca"
#SBATCH -D .
#SBATCH --output=utalca_sequential_{umin}_{max}.out
#SBATCH --error=utalca_sequential_{umin}_{max}.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
##SBATCH --gres=gpu:2
#SBATCH --time={wall_clock_limit}
./geomfinder 1EXP.pdb A 2O3P.pdb A 3 {umin} {max} 25 25 25 25 80 results/ n 50
""".format(umin=umin, max=max, wall_clock_limit=wall_clock_limit)

    # writes to file.
    filename = "utalca_sequential_" + str(umin) + "_" + str(max) + ".cmd"
    f = open(filename,"w")
    f.write(script)
    f.close()

    scripts.append(filename)

# run jobs
for s in scripts:
    subprocess.run(["sbatch", s])
