#ifndef SITE_H
#define SITE_H

/*
 * Site.h
 */

#include "utils.h"

struct Site {
    float finger_nbonded = 0.0;

    struct Distance *finger_dist;
    int finger_dist_size = 0;

    struct Distance *finger_tsp;
    int finger_tsp_size = 0;

    char *finger_asc;
    int finger_asc_size = 0;

    // amount of residues.
    int size = 0; 
    // distance where was found.
    int distance = 0; 
    
    //
    //char *str_site_numbers;
    
    char *shared;
    char *residues;
    char *residues_pocket;
};

#endif
