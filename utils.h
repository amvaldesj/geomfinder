#ifndef UTILS_H
#define UTILS_H

/*
 * utils.h
 */
 
#include <iostream>
using namespace std;

#include <string>
#include <list>
#include <vector>
#include <map>

#include "Residue.h"

/**/
struct Parameter {
    string p1;
    string ch1;
    string p2;
    string ch2;
    float radius;
    float umbralMin;
    float umbralMax;
    float nbondedPCT;
    float ascPCT;
    float tspPCT;
    float distancePCT;
    int filterMax;
    string dirOut;
    string fpocket;
    float min_drugscore;
    int nthreads;
};

/**/
struct Score {
    int x;
    int y;
    float nbe;
    float dist;
    float tsp;
    float asc;
    float final;
};

/**/
struct Coord {
   float x;
   float y;
   float z;
};

/**/
struct Distance {
    char residues[2];
    int distance;
};

/**/
struct Feature {
    struct Distance *finger_dist;
    int finger_dist_size = 0;

    vector<string> scoreASC;
    vector<string> aminos;
    float finger_nbonded = 0.0;
    float **matrix;
    int n_matrix = 0;
};

void print_finger_dist_tsp(struct Distance *finger, int size);

static map<string,float> nbonded {
    {"ASP", -1.162}, {"GLU", -1.163}, {"LYS", -1.074}, {"ARG", -0.921}, {"HIS", -1.215}, {"PRO", -1.236}, {"GLY", -1.364}, {"ALA", -1.404}, 
    {"VAL", -1.254}, {"ILE", -1.189}, {"LEU", -1.315}, {"SER", -1.297}, {"THR", -1.252}, {"MET", -1.303}, {"CYS", -1.365}, {"GLN", -1.116}, 
    {"ASN", -1.178}, {"PHE", -1.135}, {"TRP", -1.030}, {"TYR", -1.030}, {"HSD", -1.215}
};

static map<string,float> weight {
    {"ASP", 133.11}, {"GLU", 147.13}, {"LYS", 146.19}, {"ARG", 174.20}, {"HIS", 155.16}, {"PRO", 115.13}, {"GLY", 75.05}, {"ALA", 89.10}, 
    {"VAL", 117.15}, {"ILE", 131.18}, {"LEU", 131.18}, {"SER", 105.09}, {"THR", 119.12}, {"MET", 149.21}, {"CYS", 121.16}, {"GLN", 146.15}, 
    {"ASN", 132.12}, {"PHE", 165.19}, {"TRP", 204.23}, {"TYR", 181.19}, {"HSD", 155.16}
};

static map<string,string> letter_aa {
    {"ASP", "D"}, {"GLU", "E"}, {"LYS", "K"}, {"ARG", "R"}, {"HIS", "H"}, {"PRO", "P"}, {"GLY", "G"}, {"ALA", "A"}, 
    {"VAL", "V"}, {"ILE", "I"}, {"LEU", "L"}, {"SER", "S"}, {"THR", "T"}, {"MET", "M"}, {"CYS", "C"}, {"GLN", "Q"}, 
    {"ASN", "N"}, {"PHE", "F"}, {"TRP", "W"}, {"TYR", "Y"}, {"HSD", "H"},
    {"DA", "a"}, {"DC", "c"}, {"DG", "g"}, {"DT", "t"}, {"DU", "u"}
};

/*
A: Aliphatic (Glycine, Alanine, Valine, Leucine, Isoleucine)
B: Aromatic (Phenylalanine, Tyrosine, Tryptophan), 
C: OH- (Serine, Threonine)
D: Acidic (Aspartic Acid, Glutamic Acid), 
E: Acid amide (Aspargine, Glutamine), 
F: Basic (Arginine, Lysine, Histidine), 
G: Sulphur (Cysteine, Methionine) 
H: Cyclic (Proline)
X: N/A
*/
static map<string,string> category {
    {"ASP", "D"}, {"GLU", "D"}, {"LYS", "F"}, {"ARG", "F"}, {"HIS", "F"}, {"PRO", "H"}, {"GLY", "A"}, {"ALA", "A"}, 
    {"VAL", "A"}, {"ILE", "A"}, {"LEU", "A"}, {"SER", "C"}, {"THR", "C"}, {"MET", "G"}, {"CYS", "G"}, {"GLN", "E"}, 
    {"ASN", "E"}, {"PHE", "B"}, {"TRP", "B"}, {"TYR", "B"}, {"HSD", "F"},
    {"DA", "X"}, {"DC", "X"}, {"DG", "X"}, {"DT", "X"}, {"DU", "X"}
};

string trim(const string& str);
bool compare_coord(Coord a, Coord b);
bool compare_int(int a, int b);
bool compare_string(string a, string b);
bool compare_distance_node(Distance a, Distance b);
list<int> kdres_to_list(struct kdres *presults);
float *sustract_coord(float *c1, float *c2);

string get_letter_aa(string ami);
Feature get_features(map<int, Residue> residues, list<int> neigh_list);

float calc_norm(float x[],int size);
float calc_score_TD(struct Distance *finger_dist1, struct Distance *finger_dist2, int size1, int size2);
vector<Distance> calc_finger_tsp (vector<int> path, vector<string> aminos, float **matrix, int n);
float calc_score_NBE(float nbe1, float nbe2);
float calc_score_T(char *finger_asc1, char *finger_asc2, int size1, int size2);
char *calc_shared(list<list<string>> binding_sites, list<string> residues);

void minimum_cost(int city, float &cost, int n, vector<int> &path, int *visited_cities, float **matrix);
int calc_tsp(int c, float &cost, int n, int *visited_cities, float **matrix);
string get_list_string_as_string(vector<string> lstring);

#endif
