/*
 * Fpocket.cpp
 */

#include <cstring>
#include <dirent.h>

#include <regex>
#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>
#include <string>
using namespace std;

#include "Fpocket.h"
#include <cstring>

#include "utils.h"

vector<string> aminos = {"ALA", "ASP", "GLU", "LYS", "ARG", "HIS", "PRO", "GLY", "VAL", "ILE", 
                        "LEU", "SER", "THR", "MET", "CYS", "GLN", "ASN", "PHE", "TRP", "TYR"};

/**/
void apply_fpocket(Fpocket *fpocket) {
    // filename.
    fpocket->pdb_filename = fpocket->pdb.substr(fpocket->pdb.find_last_of("/\\") + 1);
    // remove extension.
    fpocket->pdbid = fpocket->pdb_filename.substr(0, fpocket->pdb_filename.find("."));
    // set result's path.
    fpocket->path = fpocket->dir_out + "/" + fpocket->pdbid + "_out/pockets/";
    // file with pockets info.
    fpocket->file_info = fpocket->dir_out + "/" + fpocket->pdbid + "_out/" + fpocket->pdbid + "_info.txt";
    
    //
    char temp[512];
    
    // set regexp.
    regex atmpdb_files("pocket.*_atm\\.pdb");
    
    // set command.
    sprintf(temp, "fpocket -f %s -o %s", fpocket->pdb.c_str(), fpocket->dir_out.c_str());
        
    // execute command.
    system((char *)temp);
    
    //
    recover_pockets_info(fpocket);
    
    std::map<string, float>::iterator itrdrug;
    
    // traverse folder.
    struct dirent *entry = nullptr;
    DIR *dp = nullptr;
    dp = opendir(fpocket->path.c_str());
    if (dp != nullptr) {
        while ((entry = readdir(dp))) {
            if (std::regex_match( entry->d_name, atmpdb_files )) {
                string filename(entry->d_name);
                string name = filename.substr(0, filename.find("_"));
                
                Pocket pocket;
                pocket.name = name;
                pocket.path = fpocket->path + filename;
            
                // find pocket name in map and get the drugscore.
                itrdrug = fpocket->pocket_info.find(pocket.name);
                pocket.drugscore = itrdrug->second;
            
                // drugscore is >= than min_drugscore?
                if ((itrdrug->second*100) >= fpocket->min_drugscore) {
                    // parse the pocket.
                    parse_pdb_pocket(pocket);
                    fpocket->pockets.push_back(pocket);
                }
            }
        }
    }
    closedir(dp);

    //
    if (fpocket->pockets.size() <= 0) {
        cout << "No pockets found." << endl;
        //exit (0);
    }
}

/**/
void recover_pockets_info(Fpocket *fpocket) {
    ifstream inFile;
    char line[100];
    
    inFile.open(fpocket->file_info);
    while (inFile) {
        inFile.getline(line, 100);
        if ((strncmp(line, "Pocket", 6) == 0)) {
            // get pocket number.
            string pock(line);
            pock = pock.substr(7, pock.find(" "));
            pock = trim(pock.substr(0, pock.find(":")));
            pock = "pocket" + pock;
            
            // get drug score.
            inFile.getline(line, 100);
            inFile.getline(line, 100);
            string drugscore(line);
            drugscore = drugscore.substr(23, drugscore.find(":"));
            
            // add info to map. ("pocketN", drugscore)
            fpocket->pocket_info.insert(pair<string, float>(pock, stof(drugscore)));
        }
    }
}

/**/
void parse_pdb_pocket(Pocket &pocket) {
    ifstream inFile;
    char line[100];
    char chain[2];
    char res_name[4];
    char res_seq_plus_iCode[6];
    list<string> residues;
    
    inFile.open(pocket.path);
    if (!inFile) {
        printf("This file: %s does not exist here, or is unreadable.\n", pocket.name.c_str());
        exit(EXIT_FAILURE);
    }
    
    while (inFile) {
        inFile.getline(line, 85);
        if ((strncmp(line, "ATOM", 4) == 0)) {
            strncpy(chain, line+21, 1);
            strncpy(res_name, line+17, 3);
            strncpy(res_seq_plus_iCode, line+22, 5);
            
            strncpy(chain+1, "\0", 1);
            strncpy(res_name+3, "\0", 1);
            strncpy(res_seq_plus_iCode+5, "\0", 1);
            
            string rname = trim(res_name);
            
            // a valid residue?
            if (find(aminos.begin(), aminos.end(), rname) != aminos.end() ) {
                string rid = trim(res_seq_plus_iCode);
                string rchain = trim(chain);
            
                string residue = rname + ":" + rid + "-" + rchain;
                residues.push_back(residue);
            }
        }
    }
    
    // sort the list.
    residues.sort();
    // removes repeated residues.
    residues.unique(compare_string);
    
    pocket.residues = residues;
}
