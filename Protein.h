#ifndef PROTEIN_H
#define PROTEIN_H

/*
 * Protein.h
 */

#include <iostream>
#include <map>
#include <list>
#include <vector>
using namespace std;

#include "Residue.h"
#include "Site.h"
#include "utils.h"
#include "Fpocket.h"

const int MIN_RESIDUES_BY_SITE=4;

/**/
struct Protein {
    string file_pdb;
    string filename;
    string chain;
    map<int, struct Residue> residues;
    map<int, struct Residue> ligands;
    struct kdtree *ptree;
    list<list<string>> binding_sites;
    vector<struct Coord> dummy_coords;
    Fpocket *fpocket;
    map<int, struct Residue> residues_pockets;
    struct kdtree *ptree_pockets;

    struct Site *array_sites;
    int total_sites;
};

struct Protein prepare_protein(string file_pdb, string chain, Parameter params);
void parse_pdb(Protein *protein);
void print_protein_data(Protein &protein);
void calc_geom_center_of_residues(Protein *protein);
float *calc_geom_center_group_r(Residue residue);
void create_kdtree_atoms_side_chain(Protein *protein);
bool atom_on_side_chain(Atom atom);
void find_binding_sites(Protein *protein);
void create_dummy_coords(Protein *protein, struct kdtree *ptree, map<int, Residue> residues, Parameter params);
void find_sites(Protein *protein, struct kdtree *ptree, map<int, Residue> residues, Parameter params);
void create_kdtree_atoms_of_pockets(Protein *protein);
void save_geomcenter_coords(Protein *protein, Parameter params);
void save_dummy_coords(Protein *protein, Parameter params);

#endif
