#ifndef FPOCKET_H
#define FPOCKET_H

/*
 * Fpocket.h
 */

#include <map>
#include <iostream>
using namespace std;

struct Pocket {
    string name;
    string path;
    list<string> residues;
    float drugscore;
};

struct Fpocket {
    vector<string> pdb_pocket_vector;
    string pdb;
    string dir_out;
    string pdb_filename;
    string pdbid;
    string path;
    string file_info;
    map<string, float> pocket_info;
    float min_drugscore;
    vector<Pocket> pockets;
};

void apply_fpocket(Fpocket *fpocket);
void recover_pockets_info(Fpocket *fpocket);
void parse_pdb_pocket(Pocket &pocket);

#endif
