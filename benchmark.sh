#!/bin/bash

umin=3
umax=5
times=1
ofile="data.txt"

# delete old data.
rm $ofile 2>/dev/null
    
# run command.
for (( counter=1; counter<=$times; counter++ ))
do
    echo "test: " $counter
    /usr/bin/time --output=$ofile --format="%e" -a ./geomfinder 1EXP.pdb A 2O3P.pdb A 3 $umin $umax 25 25 25 25 80 results/ n 50
done
    
# reads results.
sum=0
while IFS= read -r line
do
    sum=$(bc -l <<<"$sum+$line")
    echo "elapsed time: " $line
done < "$ofile"

# calculates the average.
avg=$(bc -l <<<"$sum/$times")
echo "-----------------"
echo "sum: " $sum
echo "average: " $avg
