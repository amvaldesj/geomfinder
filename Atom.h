#ifndef ATOM_H
#define ATOM_H

/*
 * Atom.h
 */

struct Atom {
    char atom_name[5];
    float coord[3];
    int	serial;
};

#endif
