/*
 * Protein.cpp
 */

#include <fstream> //ifstream
#include <cstring>
#include <string>
#include <iomanip> //std::setprecision
#include <sstream> //std::stringstream
#include <algorithm> //std::sort
#include <iostream>
using namespace std;

//
#include <chrono>
using namespace std::chrono;

#include "kdtree/kdtree.h" // https://github.com/jtsiomb/kdtree
#include <math.h>
#include <cassert>
#include "Protein.h"
#include "Residue.h"
#include "Atom.h"
#include "Site.h"
#include "Fpocket.h"

/**/
struct Protein prepare_protein(string file_pdb, string chain, Parameter params) {
    //
    struct Protein protein = Protein();
    protein.file_pdb = file_pdb;
    protein.chain = chain;
    protein.filename = protein.file_pdb.substr(protein.file_pdb.find_last_of("/\\") + 1);

    //
    parse_pdb(&protein);
    //
    calc_geom_center_of_residues(&protein);
    //
    create_kdtree_atoms_side_chain(&protein);
    //
    find_binding_sites(&protein);

    // apply fpocket?
    if (params.fpocket.compare("n") == 0) {
        // creates a grid of virtual coordinates using the list of residues.
        create_dummy_coords(&protein, protein.ptree, protein.residues, params);
        
        // searches sites.
        find_sites(&protein, protein.ptree, protein.residues, params);

    } else {
        // apply fpocket program.
        struct Fpocket *fpocket = new Fpocket();
        fpocket->pdb = file_pdb;
        fpocket->dir_out = params.dirOut;
        fpocket->pockets = {};
        fpocket->min_drugscore = params.min_drugscore;
        //
        protein.fpocket = fpocket;
        //
        apply_fpocket(fpocket);

        // creates the kdtree with all atoms in the side chain (R group) of 
        // the residues found in the pockets in the chain selected.
        create_kdtree_atoms_of_pockets(&protein);

        // creates a grid of virtual coordinates using the list of residues in the pockets.
        create_dummy_coords(&protein, protein.ptree_pockets, protein.residues_pockets, params);

        // searches sites.
        find_sites(&protein, protein.ptree_pockets, protein.residues_pockets, params);
    }
    
    // save to file.
    save_geomcenter_coords(&protein, params);
    save_dummy_coords(&protein, params);
    
    return protein;
}

/**/
void parse_pdb(Protein *protein) {
    ifstream inFile;
    char line[100] = "\0";
    float *coord = new float[3];
    coord[0] = 0.0;
    coord[1] = 0.0;
    coord[2] = 0.0;
    std::map<int, Residue>::iterator itr;
    std::map<int, Residue>::iterator itr2;
        
    // variables from the PDB file 
    int serial;
    char atom_name[5] = "\0";
    char res_name[4] = "\0";
    char res_seq_plus_iCode[6] = "\0";
    float coord_x ,coord_y ,coord_z;
    char chain[2] = "\0";
    
    // for comparison values 
    char present_res_seq_plus_iCode[6] = "\0";
    
    inFile.open(protein->file_pdb);
    if (!inFile) {
        printf("This file: %s does not exist here, or is unreadable.\n", 
            protein->file_pdb.c_str());
        exit(EXIT_FAILURE);
    }

    // initial comparison value.
    strcpy(present_res_seq_plus_iCode , ">");

    while (inFile) {
        inFile.getline(line, 85);
        
        if (inFile) {
            if ((strncmp(line, "ATOM", 4) == 0) or (strncmp(line, "HETATM", 6) == 0)) {
                strncpy(chain, line+21, 1);

                if (strcmp(chain, protein->chain.c_str()) == 0) {
                    sscanf(line+6, "%5d", &serial);
                    sscanf(line+30, "%8f", &coord_x);
                    sscanf(line+38, "%8f", &coord_y);
                    sscanf(line+46, "%8f", &coord_z);
                        
                    strncpy(atom_name, line+12, 4);
                    strncpy(res_name, line+17, 3);
                    strncpy(res_seq_plus_iCode, line+22, 5);
                        
                    strncpy(atom_name+4, "\0", 1);
                    strncpy(res_name+3, "\0", 1);
                    strncpy(chain+1, "\0", 1);
                    strncpy(res_seq_plus_iCode+5, "\0", 1);
                    
                    coord[0] = coord_x;
                    coord[1] = coord_y;
                    coord[2] = coord_z;

                    // new Atom/Hetatm
                    struct Atom atm = Atom();
                    strncpy(atm.atom_name, atom_name, 5);
                    atm.coord[0] = coord[0];
                    atm.coord[1] = coord[1];
                    atm.coord[2] = coord[2];
                    atm.serial = serial;
                    
                    if (strncmp(line, "ATOM", 4) == 0) {
                        // new Residue
                        if (strcmp(res_seq_plus_iCode, present_res_seq_plus_iCode) != 0) {
                            struct Residue res = Residue();
                            res.res_name = trim(res_name);
                            res.res_id = atoi(res_seq_plus_iCode);
                            
                            // adds new residue to the map.
                            protein->residues.insert(pair<int, Residue>(atoi(res_seq_plus_iCode), res));
                            itr = protein->residues.find(atoi(res_seq_plus_iCode));
                        }

                        strcpy(present_res_seq_plus_iCode, res_seq_plus_iCode);
                        
                        // adds ATOM to currently residue.
                        itr->second.atoms.insert(pair<int, Atom>(serial, atm));

                    } else {
                        // new Ligand (except HOH)
                        if (strcmp(res_name, "HOH") != 0) {
                            if (strcmp(res_seq_plus_iCode, present_res_seq_plus_iCode) != 0) {
                                struct Residue lig = Residue();
                                lig.res_name = trim(res_name);
                                lig.res_id = atoi(res_seq_plus_iCode);
                            
                                // adds new residue to the map.
                                protein->ligands.insert(pair<int, Residue>(atoi(res_seq_plus_iCode), lig));
                                itr2 = protein->ligands.find(atoi(res_seq_plus_iCode));
                            }

                            strcpy(present_res_seq_plus_iCode, res_seq_plus_iCode);
                        
                            // adds HETATM to currently ligand.
                            itr2->second.atoms.insert(pair<int, Atom>(serial, atm));
                        }
                    }
                }
            } else {
                // does it have models?
                if ((strncmp(line, "ENDMDL", 6) == 0)) {
                    break;
                }
            }
        }
    } // got to end of pdb file 

    // Finish off 
    inFile.close();
}

/**/
void print_protein_data(Protein &protein) {
    map<int, Residue>::iterator itr; 
    map<int, Atom>::iterator itr2; 
    
    //
    cout << "--------------------------" << endl;
    for (itr = protein.residues.begin(); itr != protein.residues.end(); ++itr) { 
        cout << protein.file_pdb << " ";
        cout << protein.chain << " ";
        cout << itr->second.res_id << " ";
        cout << itr->second.res_name << " [";
        cout << itr->second.geom_center[0] << " ";
        cout << itr->second.geom_center[1] << " ";
        cout << itr->second.geom_center[2] << "] ";
        cout << endl;

        for (itr2 = itr->second.atoms.begin(); itr2 != itr->second.atoms.end(); ++itr2) { 
            cout << "\t" << itr2->second.serial << " ";
            cout << itr2->second.atom_name << " [";
            cout << itr2->second.coord[0] << " ";
            cout << itr2->second.coord[1] << " ";
            cout << itr2->second.coord[2] << "] ";
            cout << endl;
        }
    }

    //
    for (auto bsite : protein.binding_sites) {
        for (string site : bsite) {
            cout << site << " ";
        }
        cout << endl;
    }    
}

/**/
void calc_geom_center_of_residues(Protein *protein) {
    map<int, Residue>::iterator itr; 
    float *gc;
    
    for (itr = protein->residues.begin(); itr != protein->residues.end(); ++itr) {
        gc = calc_geom_center_group_r(itr->second);
        itr->second.geom_center[0] = gc[0];
        itr->second.geom_center[1] = gc[1];
        itr->second.geom_center[2] = gc[2];
    }
}

/**/
float *calc_geom_center_group_r(Residue residue) {    
    float *coord = new float[3];
    coord[0] = coord[1] = coord[2] = 0.0;
    
    map<int, Atom> atoms = residue.atoms;
    map<int, Atom>::iterator itr;
    
    if (strcmp(residue.res_name.c_str(), "GLY") == 0) {
        for (itr = atoms.begin(); itr != atoms.end(); ++itr) {
            if (strcmp(itr->second.atom_name, " CA ") == 0) {
                coord[0] = itr->second.coord[0];
                coord[1] = itr->second.coord[1];
                coord[2] = itr->second.coord[2];
                break;
            }
        }
    }  else {
        int c = 0;
        float coordx = 0.0;
        float coordy = 0.0;
        float coordz = 0.0;
        
        for (itr = atoms.begin(); itr != atoms.end(); ++itr) {
            if (strcmp(itr->second.atom_name, " N  ") != 0 && 
                strcmp(itr->second.atom_name, " CA ") != 0 && 
                strcmp(itr->second.atom_name, " C  ")  != 0 && 
                strcmp(itr->second.atom_name, " O  ")  != 0 && 
                strcmp(itr->second.atom_name, " H  ")  != 0) {
                    
                c++;
                coordx += itr->second.coord[0];
                coordy += itr->second.coord[1];
                coordz += itr->second.coord[2];
            }
        }

        coord[0] = (coordx/c);
        coord[1] = (coordy/c);
        coord[2] = (coordz/c);
    }
    
    return coord;
}

/**/
void create_kdtree_atoms_side_chain(Protein *protein) {
    // create a k-d tree for 3-dimensional points.
    protein->ptree = kd_create(3);
    
    map<int, Residue>::iterator itr; 
    map<int, Atom>::iterator itr2; 

    // for each residue.
    for (itr = protein->residues.begin(); itr != protein->residues.end(); ++itr) {
        // gets Atoms from residue.
        map<int, Atom> atoms = itr->second.atoms;
        
        // for each atom (only side chain) of the residue.
        for (itr2 = atoms.begin(); itr2 != atoms.end(); ++itr2) {
            
            if (atom_on_side_chain(itr2->second) == true) {
                // adds tridimentional point and associates the res_id to it.
                assert (kd_insert3 (protein->ptree, 
                                    itr2->second.coord[0], 
                                    itr2->second.coord[1], 
                                    itr2->second.coord[2], 
                                    &itr->second.res_id) == 0);
            }
        }
    }
}

/**/
bool atom_on_side_chain(Atom atom) {
    if (strcmp(atom.atom_name, " N  ") != 0 && 
                strcmp(atom.atom_name, " CA ") != 0 && 
                strcmp(atom.atom_name, " C  ")  != 0 && 
                strcmp(atom.atom_name, " O  ")  != 0 && 
                strcmp(atom.atom_name, " H  ")  != 0) {
        return true;
    } else {
        return false;
    }
}

/**/
void find_binding_sites(Protein *protein) {
    map<int,Residue>::iterator it_lig;
    map<int, Atom> hetatms;
    map<int,Atom>::iterator it_het;
    map<int, Residue>::iterator itr_res;
    list<string> nearby_residues;
    struct kdres *presults;
    float origin[3];
    
    // for each ligand.
    it_lig = protein->ligands.begin();
    while (it_lig != protein->ligands.end()) {
        list<int> neigh_list;
 
        // for each hetatm of the ligand
        hetatms = it_lig->second.atoms;
        it_het = hetatms.begin();
        while (it_het != hetatms.end()) {
            origin[0] = it_het->second.coord[0];
            origin[1] = it_het->second.coord[1];
            origin[2] = it_het->second.coord[2];
            
            // gets neighbors.
            presults = kd_nearest_range(protein->ptree, origin, 6);
            
            // converts results to a list. (ORDERED and UNIQUEs)
            list<int> tmp_list = kdres_to_list(presults);
            
            // adds to tmp_list to neigh_list.
            neigh_list.insert(neigh_list.end(), tmp_list.begin(), tmp_list.end());
            
            it_het++;
        }
        
        neigh_list.sort();
        neigh_list.unique(compare_int);
        
        if (neigh_list.size() > 0) {
            // for each neighly residue.
            list<string> bsite;
            bsite.push_back(it_lig->second.res_name + ":" + to_string(it_lig->second.res_id));
            for (auto res_id : neigh_list) {
                // get residue data.
                itr_res = protein->residues.find(res_id);
                bsite.push_back(itr_res->second.res_name + to_string(itr_res->second.res_id));
            }
            
            protein->binding_sites.push_back(bsite);
        }
        it_lig++;
    }
}

/**/
void create_dummy_coords(Protein *protein, struct kdtree *ptree, map<int, Residue> residues, Parameter params) {
    list<Coord> tmp_dummy;
    map<int, Residue>::iterator itr; 
    map<int, Residue>::iterator itr2;
    float pos[3];
    
    // for each Residue.
    for (itr = residues.begin(); itr != residues.end(); ++itr) {
        // gets geometrical center of residue.
        float origin[3] = {itr->second.geom_center[0], 
                            itr->second.geom_center[1], 
                            itr->second.geom_center[2]};
        
        // gets neighbors.
        struct kdres *presults = kd_nearest_range(ptree, origin, params.radius);
        
        // for each neighbor calculates the media point with the currently residue.
        if (kd_res_size(presults) > 0) {
            map<int,int> tmp_res;
            
            while (!kd_res_end (presults)) {
                // get data from the neighbor residue.
                int *res_id = (int *) kd_res_item(presults, pos);
                
                itr2 = residues.find(*res_id);
                    
                // the residues can be repeated. is new?
                if (tmp_res.find(itr2->first) == tmp_res.end() ) {
                    tmp_res.insert(pair<int, int>(itr2->first, itr2->first));
                        
                    // creates dummy coords.
                    Coord dummy = Coord();
                    dummy.x = (origin[0] + itr2->second.geom_center[0])/2;
                    dummy.y = (origin[1] + itr2->second.geom_center[1])/2;
                    dummy.z = (origin[2] + itr2->second.geom_center[2])/2;

                    tmp_dummy.push_back(dummy);
                }
                kd_res_next(presults);
            }
        } else {
            // creates dummy coords with geom center.
            Coord dummy = Coord();
            dummy.x = origin[0];
            dummy.y = origin[1];
            dummy.z = origin[2];
            tmp_dummy.push_back(dummy);
        }
        
        kd_res_free(presults);
    }
    
    // uniques coordinates.
    tmp_dummy.unique(compare_coord);
    // copy list to vector.
    vector<Coord> v { make_move_iterator(begin(tmp_dummy)), make_move_iterator(end(tmp_dummy)) };
    protein->dummy_coords = v;
}

/**/
void find_sites(Protein *protein, struct kdtree *ptree, map<int, Residue> residues, Parameter params) {
    //
    chrono::duration<float> elapsed;
    auto start = chrono::high_resolution_clock::now();

    //
    //vector<Site*> all_v_sites;
    Site *sites = NULL;
    int n_sites = 0;
    
    map<int, Residue>::iterator itr;
    map<string,int> tmp_sites;
    map<string,int> tmp_scoreASC;
    
    // the number of dummy coordinates.
    int v_size = protein->dummy_coords.size();
    
    for (int i=0; i<v_size; i++) {
        for (int dist = params.umbralMin; dist <= (int) params.umbralMax; dist++) {
#ifdef TRACING
Extrae_event(104, dist+1);
#endif             
            //
            float origin[3] = { protein->dummy_coords[i].x, 
                                protein->dummy_coords[i].y, 
                                protein->dummy_coords[i].z };
                
            // gets the neighbors.
            struct kdres *presults = kd_nearest_range(ptree, origin, dist);
                
            // converts results (with residues IDs) to a list. (sorted and unique)
            list<int> neigh_list = kdres_to_list(presults);
                
            kd_res_free(presults);
                
            // at least MIN_RESIDUES_BY_SITE (4) residues?.
            if (neigh_list.size() >= MIN_RESIDUES_BY_SITE) {
                string str_site_numbers = "\0";
                list<string> site_list_residues;
                list<string> site_list_residues_pocket;
                    
                // for each neighbor residue ID.
                for (auto res_id: neigh_list) {
                    // gets the neighbor (residue) data from the map.
                    itr = residues.find(res_id);
                        
                    // creates a temporal string copy of residue ID (res_id).
                    string tmp = to_string(res_id);
                        
                    // a string representation of the site to control repetitions.
                    if (str_site_numbers == "\0")
                        str_site_numbers = str_site_numbers + tmp;
                    else
                        str_site_numbers = str_site_numbers + ":" + tmp;
                        
                    // adds residue in the list of residues (residue name + res_id).
                    site_list_residues.push_back(itr->second.res_name + tmp);

                    // it is not necessary if fpocket hasn't been selected.
                    if (params.fpocket.compare("y") == 0) {
                        // list of residues + pocket + drugscore.
                        float drugscore = itr->second.drugscore * 100;
                        
                        std::stringstream stream;
                        stream << std::fixed << std::setprecision(1) << drugscore;
                        std::string dscore = stream.str();
                        
                        site_list_residues_pocket.push_back(itr->second.res_name + 
                            tmp + "-" + itr->second.pocket_name + "[" + dscore + "%]");
                    }
                }

                // the site can be repeated. is new str_site_numbers in the map?
                if (tmp_sites.find(str_site_numbers) == tmp_sites.end()) {
                    
                    char *char_site_numbers = new char[str_site_numbers.length() + 1];
                    strcpy(char_site_numbers, str_site_numbers.c_str());
                        
                    // calculates features of the site.
                    Feature feature = get_features(residues, neigh_list);

                    // gets a string with the sequence of the site's components (each residue 
                    // is tagged into a category).
                    string str_scoreASC = get_list_string_as_string(feature.scoreASC);
                        
                    tmp_scoreASC.insert(pair<string, int>(str_scoreASC, 0));
                    tmp_sites.insert(pair<string, int>(str_site_numbers, 0));
      
                    // calculates TSP (distances constituting the shortest pathway 
                    // necessary to go over all the residues of the site).
                    vector<int> path;
                    float cost = 0;
                    int *visited_residues = new int [feature.n_matrix];
                    for (int i=0; i<feature.n_matrix; i++) {
                        visited_residues[i] = 0;
                    }
                    // paramaters by reference. 
                    minimum_cost(0, cost, feature.n_matrix, path, visited_residues, feature.matrix);
 
                    //
                    vector<Distance> finger_tsp = calc_finger_tsp(path, 
                        feature.aminos, 
                        feature.matrix, 
                        feature.n_matrix);
 
#ifdef OPT
                    // sort elements.
                    sort(finger_tsp.begin(), finger_tsp.end(), compare_distance_node);
#endif
                    int total_finger_tsp = finger_tsp.size();
                    struct Distance *s_finger_tsp = new struct Distance[total_finger_tsp];
                    for (int i=0; i<total_finger_tsp; i++) {
                        s_finger_tsp[i] = finger_tsp[i];
                    }

                    //
                    string tmp_finger_asc = "\0";
                    for (auto item: feature.scoreASC) {
                        if (tmp_finger_asc == "\0")
                            tmp_finger_asc = item;
                        else
                            tmp_finger_asc = tmp_finger_asc + item;
                    }
#ifdef OPT
                    // sort elements.
                    sort(tmp_finger_asc.begin(), tmp_finger_asc.end());
#endif
                    char *s_finger_asc = new char[tmp_finger_asc.length() + 1];
                    strcpy(s_finger_asc, tmp_finger_asc.c_str());
                    int total_finger_asc = tmp_finger_asc.length();

                    //
                    char *shared = calc_shared(protein->binding_sites, site_list_residues);

                    //
                    string tmp_residues = "\0";
                    for (auto item: site_list_residues) {
                        if (tmp_residues == "\0")
                            tmp_residues = item;
                        else
                            tmp_residues = tmp_residues + " " + item;
                    }
                    char *residues = new char[tmp_residues.length() + 1];
                    strcpy(residues, tmp_residues.c_str());

                    //
                    string tmp_residues_pocket = "\0";
                    for (auto item: site_list_residues_pocket) {
                        if (tmp_residues_pocket == "\0")
                            tmp_residues_pocket = item;
                        else
                            tmp_residues_pocket = tmp_residues_pocket + " " + item;
                    }
                    char *residues_pocket = new char[tmp_residues_pocket.length() + 1];
                    strcpy(residues_pocket, tmp_residues_pocket.c_str());

                    // create struct Site.
                    sites = (Site *) realloc(sites, (n_sites+1)*sizeof(Site));
                    sites[n_sites].finger_nbonded = feature.finger_nbonded;
                    sites[n_sites].finger_dist_size = feature.finger_dist_size;
                    sites[n_sites].finger_dist = feature.finger_dist;
                    sites[n_sites].finger_tsp_size = total_finger_tsp;
                    sites[n_sites].finger_tsp = s_finger_tsp;
                    sites[n_sites].finger_asc = s_finger_asc;
                    sites[n_sites].finger_asc_size = total_finger_asc;
                    sites[n_sites].size = neigh_list.size();
                    sites[n_sites].distance = dist;
                    sites[n_sites].shared = shared;
                    sites[n_sites].residues = residues;
                    sites[n_sites].residues_pocket = residues_pocket;

                    n_sites++;
                }
            }
#ifdef TRACING
Extrae_event(104, 0);
#endif             
        }
    }
    //
    protein->total_sites = n_sites;
    protein->array_sites = sites;

    //
    elapsed = chrono::high_resolution_clock::now() - start;
    cout << "time find_sites: " << protein->file_pdb << " " << elapsed.count() << " seconds" << endl;
}

/**/
void create_kdtree_atoms_of_pockets(Protein *protein) {
    string resid;
    std::map<int, Residue>::iterator itrp;
    std::map<int, Residue>::iterator itrpocket;
    
    // gets residue objects from residue pocket ID.
    for (struct Pocket pocket: protein->fpocket->pockets) {
        for (string r: pocket.residues) {
            // obtain only residue ID number.
            resid = r.substr(4, r.find("-"));
            resid = resid.substr(0, resid.find("-"));
            // obtain de chain.
            string ch = r.substr(r.find("-") + 1, -1);
            
            // the same chain.
            if (strcmp(protein->chain.c_str(), ch.c_str()) == 0) {
                // convert to int.
                int tid = stoi(resid);
            
                // get residue from map of residues of structure.
                itrp = protein->residues.find(tid);

                // found residue?
                if (itrp != protein->residues.end()) {
                    // already exists?
                    itrpocket = protein->residues_pockets.find(tid);
                    if (itrpocket == protein->residues_pockets.end()) {
                        Residue rp = itrp->second;
                        // set pocket name.
                        //rp.set_pocket_name(pocket.name);
                        rp.pocket_name = pocket.name;

                        // set pocket drugscore.
                        //rp.set_drugcore(pocket.drugscore);
                        rp.drugscore = pocket.drugscore;

                        // adds residue to map of pockets residues.
                        protein->residues_pockets.insert(pair<int, Residue>(tid, rp));
                    }
                } else {
                    printf("create_kdtree_atoms_of_pockets: id residue: %d not found!\n", tid);
                }
            }
        }
    }
    
    // create a k-d tree for 3-dimensional points.
    protein->ptree_pockets = kd_create(3);
    
    map<int, Residue>::iterator itr; 
    map<int, Atom>::iterator itr2; 
    
    // for each residue on pockets.
    for (itr = protein->residues_pockets.begin(); itr != protein->residues_pockets.end(); ++itr) {
        // gets Atoms from residue.
        map<int, Atom> atoms = itr->second.atoms;

        // for each atom (only side chain) of the residue.
        for (itr2 = atoms.begin(); itr2 != atoms.end(); ++itr2) {
            
            if (atom_on_side_chain(itr2->second) == true) {
                // adds tridimentional point and associates the res_id to it.
                assert (kd_insert3 (protein->ptree_pockets, 
                                    itr2->second.coord[0], 
                                    itr2->second.coord[1], 
                                    itr2->second.coord[2], 
                                    &itr->second.res_id) == 0);
            }
        }
    }
}

/**/
void save_geomcenter_coords(Protein *protein, Parameter params) {
    FILE *output;
    string filename = params.dirOut + "/" + protein->filename+ "-geomcenter-coord.pdb";
    
    output = fopen(filename.c_str(), "w");
    
    string name = "DA";
    string resname = "DA";
    string chain = "A";
    string segname = "A";
    int resid = 1;    
    map<int, Residue>::iterator itr; 
    
    for (itr = protein->residues.begin(); itr != protein->residues.end(); ++itr) { 
        Residue *residue = &itr->second;
        
        fprintf (output, "ATOM  %5d  %-3s %-4s%s%4d    %8.3f%8.3f%8.3f %5.2f %5.2f     %-4s\n", 
            resid, name.c_str(), resname.c_str(), 
            chain.c_str(), resid, residue->geom_center[0], residue->geom_center[1], residue->geom_center[2], 
            1.0, 1.0, segname.c_str());

        resid++;
    }
    
    fprintf (output, "END\n");
    
    fclose(output);
}

/**/
void save_dummy_coords(Protein *protein, Parameter params) {
    FILE *output;
    string filename = params.dirOut + "/" + protein->filename + "-dummy-coord.pdb";
    
    output = fopen(filename.c_str(), "w");
    
    string name = "DA";
    string resname = "DA";
    string chain = "A";
    string segname = "A";
    int resid = 1;
    
    for (Coord coor: protein->dummy_coords) {
        fprintf (output, "ATOM  %5d  %-3s %-4s%s%4d    %8.3f%8.3f%8.3f %5.2f %5.2f     %-4s\n", 
            resid, name.c_str(), resname.c_str(), 
            chain.c_str(), resid, 
            coor.x, coor.y, coor.z, 
            1.0, 1.0, segname.c_str());

        resid++;
    }
    fprintf (output, "END\n");
    
    fclose(output);
}
