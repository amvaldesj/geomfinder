/*
 * utils.cpp
 */

#include <iostream>
using namespace std;
#include <string>
#include <list>
#include <vector>
#include <cstring>
#include <cmath>
#include <bits/stdc++.h> 

#include "kdtree/kdtree.h"
#include "Protein.h"
#include "utils.h"

void print_finger_dist_tsp(struct Distance *finger, int size) {
    for (int i=0; i<size; i++) {
        struct Distance node = finger[i];
        printf("%c%c%d-", node.residues[0], node.residues[1], node.distance);
    }
    printf("\n");
}

/*
 */
#ifdef OPT
float calc_score_TD(struct Distance *finger_dist1, struct Distance *finger_dist2, \
    int size1, int size2) {
    
    // example AC5: node.residue[0]=A node.residue[1]=C node.distance=5
    //
    int similar = 0;
    float score = 0.0;
    int pos = 0;
    
    // for each node in finger_dist1.
    for (int i=0; i<size1; i++) {
        struct Distance node1 = finger_dist1[i];

        for (int j=pos; j<size2; j++) {
            struct Distance node2 = finger_dist2[j];
            
            // both are minor, so go to the next iteration.
            if (tie(node2.residues[0], node2.residues[1]) < tie(node1.residues[0], node1.residues[1])) 
                continue;
            
            // both are the same.
            if (tie(node2.residues[0], node2.residues[1]) == tie(node1.residues[0], node1.residues[1])) {
                // so compare the distances.
                if (abs(node2.distance - node1.distance) <= 2) {
                    similar++;
                    pos = j + 1;
                    break;
                }
            }
        }
    }
    
    score =  similar/(float)(max(size1, size2));

    return score;
}
#else
float calc_score_TD(struct Distance *finger_dist1, struct Distance *finger_dist2, int size1, int size2) {
    //
    int similar = 0;
    float score = 0.0;

    // initialize all values to 0
    int *revised = new int[size2] {0};
    
    
    // for each node in finger_dist1.
    for (int i=0; i<size1; i++) {
        struct Distance node1 = finger_dist1[i];

        for (int j=0; j<size2; j++) {
            struct Distance node2 = finger_dist2[j];
            
            if ((revised[j] == 0) and
                (node1.residues[0] == node2.residues[0]) and 
                (node1.residues[1] == node2.residues[1]) and 
                (abs(node1.distance - node2.distance) <= 2)) {

                similar = similar + 1; 
                
                // exclude this value of the comparison.
                revised[j] = 1;
                
                break;
            }
        }
    }
    
    score =  similar/(float)(max(size1, size2));
    delete [] revised;
    return score;
}
#endif

/**/
string trim(const string& str) {
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first) {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

/**/
bool compare_string(string a, string b) {
    if (a.compare(b) == 0)
        return true;
    else
        return false;
}

/**/
bool compare_coord(Coord a, Coord b) { 
    return ((float)a.x == (float)b.x and (float)a.y == (float)b.y and (float)a.z == (float)b.z); 
} 

/**/
bool compare_distance_node(Distance a, Distance b) {
    return tie(a.residues[0], a.residues[1], a.distance) < tie(b.residues[0], b.residues[1], b.distance);
}

/**/
bool compare_int(int a, int b) { 
    return ((a)==(b)); 
} 

/**/
Feature get_features(map<int, Residue> residues, list<int> neigh_list) {
    map<int, Residue>::iterator itr;
    map<int, Residue>::iterator itr2;
    
    float finger_nbonded = 0.0;
    std::map<string, float>::iterator itr_nbonded;
    std::map<string, float>::iterator itr_weight;
    std::map<string, string>::iterator itr_category;
    vector<string> scoreASC;
    int i = 0;
    long unsigned int j = 0;
    vector<string> aminos;
    int distance;

    vector<Distance> finger_dist;
    
    // initializes matrix with zeros.
    int n = neigh_list.size();
    float **matrix;
    matrix = new float *[n];
    
    for(int ii = 0; ii<n; ii++)
        matrix[ii] = new float[n];
    for (int ii=0; ii<n; ii++) 
        for (int jj=0; jj<n; jj++)
            matrix[ii][jj] = 0.0;
    
    // for each residue.
    for (auto res_id: neigh_list) {
        itr = residues.find(res_id);
        
        float coor_res1[3] = {  itr->second.geom_center[0], 
                                itr->second.geom_center[1], 
                                itr->second.geom_center[2] };
        
        // get values from maps.
        //FIXME: How to calculate to nucleotide (DA, DT, etc)?
        itr_nbonded = nbonded.find(itr->second.res_name);
        itr_weight = weight.find(itr->second.res_name);
        finger_nbonded = finger_nbonded + ((itr_nbonded->second + 1) * itr_weight->second);
        
        // get values from maps.
        itr_category = category.find(itr->second.res_name);
        
        if (itr_category ==  category.end()) {
            cout << "Error, aminoacid not found: " <<  res_id  << " " << itr->second.res_name << endl;
            exit(EXIT_FAILURE);
        }
        
        scoreASC.push_back(itr_category->second);
        for (auto res_id2: neigh_list) {
            itr2 = residues.find(res_id2);
            
            float coor_res2[3] = {  itr2->second.geom_center[0], 
                                    itr2->second.geom_center[1], 
                                    itr2->second.geom_center[2] };
            
            float *diff = sustract_coord(coor_res1, coor_res2);
            float beta_dist = calc_norm(diff, 3);
            
            if (j < (neigh_list.size() - 1)) {
                matrix[j][i] = beta_dist;
                j++;
            } else {
                matrix[j][i] = beta_dist;
                aminos.push_back(itr->second.res_name + to_string(itr->second.res_id));
                j = 0;
                i++;
            }
            
            // are different residues?
            if (itr->second.res_id != itr2->second.res_id) {
                string ab = "\0";

                // creates an ordered string.
                if (letter_aa[itr->second.res_name] <= letter_aa[itr2->second.res_name]){
                    ab = letter_aa[itr->second.res_name] + letter_aa[itr2->second.res_name];
                } else {
                    ab = letter_aa[itr2->second.res_name] + letter_aa[itr->second.res_name];
                }
                
                // round the distance.
                distance = (int) round(beta_dist);
                Distance d_node = Distance();
                strcpy(d_node.residues, ab.c_str());
                d_node.distance = distance;
                finger_dist.push_back(d_node);
            }
        }
    }

#ifdef OPT
    // sort elements.
    sort(finger_dist.begin(), finger_dist.end(), compare_distance_node);
#endif

    // copy and remove duplicated.
    int total_finger_dist = finger_dist.size()/2;
    int idx = 0;
    struct Distance *s_finger_dist = (struct Distance*) malloc(total_finger_dist * sizeof(struct Distance));
    for (int i=0; i<total_finger_dist; i++) {
        idx = (i*1)+i;
        s_finger_dist[i] = finger_dist[idx];
    }

    //
    Feature f = Feature();
   
    f.finger_dist = s_finger_dist;
    f.finger_dist_size = total_finger_dist;
    f.scoreASC = scoreASC;
    f.finger_nbonded = finger_nbonded;
    f.aminos = aminos;
    f.matrix = matrix; // distances matrix among all aminos.
    f.n_matrix = n;
        
    return f;
}

/**/
float *sustract_coord(float *c1, float *c2) {
    float *res;
    res = new float[3];
    for (int i=0; i<3 ;i++){
        res[i] = c1[i] - c2[i];
    }
    return res;
}

/**/
float calc_norm(float x[],int size) {
  int  total = 0;
  for (int i = 0; i < size; ++i) {
    total += x[i] * x[i];
  }
  return sqrt(total);
}

/*
 * return an ordered and unique list of integers of residues IDs.
 */
list<int> kdres_to_list(struct kdres *presults) {
    list<int> lst;
    float pos[3];

    // sets at beginning of the results.
    kd_res_rewind(presults);
    
    // traversal the results.
    while (!kd_res_end (presults)) {
        int *tmp = (int*) kd_res_item(presults, pos);
        if (*tmp != 0) {
            //printf("%d\n", *tmp);
            lst.push_back(*tmp);
        }
        kd_res_next(presults);
    }
    
    lst.sort();
    lst.unique(compare_int);
    return lst;
}

/*
 * return the one-letter code of residue.
 */
string get_letter_aa(string ami) {
    std::map<string, string>::iterator itr_letter_aa;
    
    itr_letter_aa = letter_aa.find(ami);
    
    if (itr_letter_aa != letter_aa.end()) {
        return (itr_letter_aa->second);
    } else {
        cout << "unknown residue: " << ami << endl;
        exit(EXIT_FAILURE);
    }
        
}

/**/
char *calc_shared(list<list<string>> binding_sites, list<string> residues) {    
    list<string> similars;
    
    residues.sort();
    
    for (auto bsite : binding_sites) {
        // gets the drug of the site.
        string drug = bsite.front();
        
        // remove the drug of list.
        bsite.pop_front();
        
        bsite.sort();
        
        // find common residues.
        list<string> shared;
        set_intersection(residues.begin(), residues.end(), bsite.begin(), 
            bsite.end(), back_inserter(shared));
        
        if (shared.size() > 0) {
            shared.sort();
            
            string similar = "\0";
            for (auto res : shared) {
                if (similar == "\0")
                    similar = res;
                else
                    similar = similar + ":" + res;
            }
            similar = drug + "-" + similar;
            similars.push_back(similar);
        }
    }
    
    string total_similar = "\0";
    if (similars.size() > 0) {
        for (auto sha : similars) {
            if (total_similar == "\0")
                total_similar = sha;
            else
                total_similar = total_similar + "|" + sha;
        }
    } 
    
    char *cstr = new char[total_similar.length() + 1];
    strcpy(cstr, total_similar.c_str());

    return cstr;
}

/**/
int tsp(int c, float &cost, int n, int *visited_cities, float **matrix) {
    int count, nearest_city = 999;
    float minimum = 999, temp = 0;
      
    for (count = 0; count < n; count++) {
        if ((matrix[c][count] != 0) && (visited_cities[count] == 0)) {
            if (matrix[c][count] < minimum) {
                minimum = matrix[c][count];
                temp = matrix[c][count];
                nearest_city = count;
            }
        }
    }
    
    if (minimum != 999) {
        cost = cost + temp;
    }
    
    return nearest_city;
}

/**/
void minimum_cost(int city, float &cost, int n, vector<int> &path, int *visited_cities, float **matrix) {
    int nearest_city;
    visited_cities[city] = 1;
    path.push_back(city);
    
    nearest_city = tsp(city, cost, n, visited_cities, matrix);
      
    if (nearest_city == 999) {
        nearest_city = 0;
        path.push_back(nearest_city);
        cost = cost + matrix[city][nearest_city];
        return;
    }
    
    minimum_cost(nearest_city, cost, n, path, visited_cities, matrix);
}

/**/
float calc_score_NBE(float nbe1, float nbe2) {
    float num = min(abs(nbe1), abs(nbe2));
    float den = max(abs(nbe1), abs(nbe2)); 
    float scoredistf = (num/den);
    
    if ((nbe1 > 0 and nbe2 < 0) or (nbe1 < 0 and nbe2 > 0)) {
        scoredistf = 1.0 - scoredistf;
    }
    
    return scoredistf;
}

/*
 * finger_asc1/finger_asc2 = D,B,A ...
 */
#ifdef OPT
float calc_score_T(char *finger_asc1, char *finger_asc2, int size1, int size2) {
    
    // example ABC: finger[0]=A finger[1]=B finger[2]=C
    //
    float score = 0;
    int similar = 0;
    int pos = 0;

    // for each char in finger_asc1.
    for (int i=0; i<size1; i++) {
        for (int j=pos; j<size2; j++) {
            
            if (finger_asc2[j] < finger_asc1[i]) continue;
            
            if (finger_asc2[j] == finger_asc1[i]) {
                similar = similar + 1;
                pos = j + 1;
                break;
            }
        }
    }

    score =  similar/(float)(max(size1, size2));    

    return score;
}
#else
float calc_score_T(char *finger_asc1, char *finger_asc2, int size1, int size2) {
    //
    float score = 0;
    int similar = 0;

    // initialize all values to 0
    int *revised = new int[size2] {0};

    // for each char in finger_asc1.
    for (int i=0; i<size1; i++) {
        for (int j=0; j<size2; j++) {
            if ((revised[j] == 0) and
                (finger_asc1[i] == finger_asc2[j])) {

                similar = similar + 1;

                // exclude this value of the comparison.
                revised[j] = 1;
                break;
            }
        }
    }

    score =  similar/(float)(max(size1, size2));    

    delete [] revised;

    return score;
}
#endif

/**/
vector<Distance> calc_finger_tsp (vector<int> path, vector<string> aminos, float **matrix, int n) {
    int u = 0;
    int i = 0;
    int j = 0;
    vector<Distance> finger_tsp;
    string aa1;
    string aa2;
    string aa_bb;
    int dist;
    int pos1 = 3;
    int pos2 = 3;
    
    while (u < (n - 1)) {
        j = path.at(u);
        i = path.at(u+1);
        
        aa1 = aminos[j];
        aa2 = aminos[i];
        
        // exceptions
        if (aa1.substr(0, 1).compare("D") == 0) {
            pos1 = 2;
        }
        else {
            pos1 = 3;
        }
        
        // exceptions
        if (aa2.substr(0, 1).compare("D") == 0) {
            pos2 = 2;
        } else {
            pos2 = 3;
        }
        
        // sorts the string.
        if (get_letter_aa(aa1.substr(0, pos1)) <= get_letter_aa(aa2.substr(0, pos2))){
            aa_bb = get_letter_aa(aa1.substr(0, pos1)) + get_letter_aa(aa2.substr(0, pos2));
        } else {
            aa_bb = get_letter_aa(aa2.substr(0, pos2)) + get_letter_aa(aa1.substr(0, pos1));
        }
        
        //
        dist = (int) round(matrix[i][j]);
        Distance d_node = Distance();
        strcpy(d_node.residues, aa_bb.c_str());
        d_node.distance = dist;
        finger_tsp.push_back(d_node);

        u += 1;
    }
    
    return finger_tsp;
}

/**/
string get_list_string_as_string(vector<string> lstring) {
    string lres = "\0";
    for (auto item: lstring) {
        
        if (lres == "\0")
            lres = item;
        else
            lres = lres + ":" + item;
    }
    return lres;
}
