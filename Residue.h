#ifndef RESIDUE_H
#define RESIDUE_H

/*
 * Residue.h
 */

#include <iostream>
using namespace std;
#include <map>

#include "Atom.h"

struct Residue {
    int res_id; 
    string res_name;
    map<int, Atom>  atoms;
    float geom_center[3] = {0.0, 0.0, 0.0};
    string pocket_name;
    float drugscore;    
};

#endif
